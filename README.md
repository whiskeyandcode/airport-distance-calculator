# Airport distance calculator


The code is organized into:

*   apps/ - Where all apps reside
*   static/ - Our static files directory
*   distance_calculator/ - where our manage.py is.
*   conf/ - where our environment specific settings will live (ex. production, integration, etc)
*   bin/ - where we have a script to create the base site (Usage: `setup-site.py <site-name>`)


You should setup your virtualenv in the root directory.

## Usage

### Usage without docker 

*   `cd <checkout-location>`
*   `virtualenv env`
*   `source env/bin/activate`
*   `pip install -r requirements.txt`
*   Create a database in mysql using: `create database dist2airport`
*   `./manage.py migrate`
*   `./manage.py runserver`

### Usage with docker
    
*   Install docker and docker-compose
*   Ensure you have the docker machine's IP address
*   Run `docker-compose up`
*   Go to <machine-ip>:8000 in your favorite browser


## Environment import order

There are many different ways to organize environment specific settings
Settings are imported in this order so that deployment to servers becomes easier.
However edit `manage.py` and `<site_name>/settings/__init__.py` if you want to manage it in another way.

Here's how the order goes:

1. `conf/settings_override/default/`
2. `<site_name>/settings/`
3. `conf/settings_override/<env>/`
