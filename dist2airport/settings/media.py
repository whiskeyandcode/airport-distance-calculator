import os

SITE_NAME = os.environ['DJANGO_SITE_NAME']
APP_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../'))
ADMIN_MEDIA_PREFIX = '/media/'

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(APP_ROOT, 'upload')
STATIC_ROOT = os.path.join(APP_ROOT, 'static')


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://cdn.example.com", "http://example.com/media/"
MEDIA_URL = '/upload/'
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)
COMPRESS_OUTPUT_DIR = 'dist2airport'
