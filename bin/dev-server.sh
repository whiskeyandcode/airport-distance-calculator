#!/bin/bash

set -e

host="$1"
shift

function check_mysql(){
    curl -s db:3306 >/dev/null && return 0 || return 1
}

until check_mysql; do
  echo "Mysql is unavailable - sleeping"
  sleep 1
done
echo "Mysql is up - executing command"

cd /code
./manage.py migrate
./manage.py loaddata apps/airports/fixtures/us_airports.json
./manage.py runserver 0.0.0.0:8000