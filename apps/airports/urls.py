from django.conf.urls import url
from airports import views

urlpatterns = [
    url(r'^search/?$', views.AirportList.as_view(),
        name='search-airports'),
    url(r'^distance/?$', views.CalculateDistanceView.as_view(),
        name='get-distance')
]
