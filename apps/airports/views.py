from rest_framework import generics, status, views
from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from django.db.models import Q
from geopy.distance import great_circle
from airports.serializers import AirportSerializer, DistanceCalculatorSerializer
from airports.models import Airport


class AirportList(generics.ListAPIView):
    # query U.S Airports only
    queryset = Airport.objects.us_airports()
    serializer_class = AirportSerializer
    pagination_class = pagination.LimitOffsetPagination

    def list(self, request):
        q = request.GET.get('q')
        queryset = self.get_queryset()
        if q:
            queryset = queryset.filter(
                Q(name__icontains=q) |
                Q(city__icontains=q) |
                Q(faa_code__icontains=q))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CalculateDistanceView(views.APIView):
    parser_classes = (JSONParser, FormParser, MultiPartParser)

    def post(self, request, format=None):
        serializer = DistanceCalculatorSerializer(data=request.data)
        if serializer.is_valid():
            # if the input is clean, calculate the distance
            loc1 = (serializer.data.get('lat1'), serializer.data.get('lon1'))
            loc2 = (serializer.data.get('lat2'), serializer.data.get('lon2'))
            distance = "%.2f" % great_circle(loc1, loc2).miles
            return Response({
                'distance': distance,
                'description': "in miles"
            })

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
