from rest_framework import serializers
from airports.models import Airport


class AirportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Airport
        fields = ('name', 'city', 'faa_code', 'latitude', 'longitude')
        # fields = ('airport_id', 'name', 'city', 'country', 'faa_code', 'icao_code', 'latitude', 'longitude', 'altitude', 'timezone', 'dst', 'tz_info')


class DistanceCalculatorSerializer(serializers.Serializer):
    lat1 = serializers.DecimalField(max_digits=9, decimal_places=6)
    lon1 = serializers.DecimalField(max_digits=9, decimal_places=6)
    lat2 = serializers.DecimalField(max_digits=9, decimal_places=6)
    lon2 = serializers.DecimalField(max_digits=9, decimal_places=6)
