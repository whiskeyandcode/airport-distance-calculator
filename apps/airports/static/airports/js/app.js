$(function(){
    var start, end;
    var app_services = (function(){
        var search_url = $('#from_airport').attr('data-autocomplete-url');
        var calc_distance_url = $('#get_dist_form').attr('action');
        var _search = function(request, response){
            $.ajax({
                url: search_url, 
                data: {
                    q: request.term
                }, 
                success: function(data){
                    var airports = $.map(data.results, function(el){
                        return {
                            'label': el.name + '/' + el.city,
                            'value': el.name + '/' + el.city,
                            'hit': el
                        };
                    });
                    response(airports);
                },
                error: function(){
                    response([]);
                }
            });
        };

        var _get_dist = function(){
            return $.post(calc_distance_url, $("#get_dist_form").serialize(), "json");
        };
        return {
            search: _search,
            get_distance: _get_dist
        };
    })();

    // Initialize autocomplete 
    $("#from_airport").autocomplete({
        source: app_services.search,
        minLength: 2,
        select: function( event, ui ) {
            $('.message').addClass('hidden');
            var el = ui.item.hit;
            // set the lat/lon for the form elements
            $('#id_lat1').val(el.latitude);
            $('#id_lon1').val(el.longitude);
            $('#from_label').text(ui.item.label);
            start = {
                lat: parseFloat(el.latitude),
                lng: parseFloat(el.longitude)
            };
        }
    });

    // Initialize autocomplete
    $("#to_airport").autocomplete({
        source: app_services.search,
        minLength: 2,
        select: function( event, ui ) {
            $('.message').addClass('hidden');
            var el = ui.item.hit;
            // set the lat/lon for the form elements
            $('#id_lat2').val(el.latitude);
            $('#id_lon2').val(el.longitude);
            $('#to_label').text(ui.item.label);
            end = {
                lat: parseFloat(el.latitude),
                lng: parseFloat(el.longitude)
            };
        }
    });

    /* Event listeners */
    $('body').delegate('#calculate_distance', 'click', function(){
        app_services.get_distance().done(function(data, status, jQxhr){
            // show the distance
            // this can be done in a better way with angular's databinding features
            $('#distance_label').text(data.distance);
            $('.message').removeClass('hidden');
            mapapp.reinitMap(start, end);
        });
    });
});
