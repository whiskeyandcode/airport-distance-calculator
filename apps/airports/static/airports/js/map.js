// Code courtesy developers.google.com/maps/documentation/javascript/examples/geometry-headings
window.mapapp = (function(){
  var marker1, marker2;
  var geodesicPoly;

  function _reinitMap(start, end) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(
          (start.lat + end.lat)/2, 
          (start.lng + end.lng)/2)
    });
   
    marker1 = new google.maps.Marker({
      map: map,
      draggable: false,
      position: start
    });

    marker2 = new google.maps.Marker({
      map: map,
      draggable: false,
      position: end
    });

    geodesicPoly = new google.maps.Polyline({
      strokeColor: '#CC0099',
      strokeOpacity: 1.0,
      strokeWeight: 3,
      geodesic: true,
      map: map
    });

    var path = [marker1.getPosition(), marker2.getPosition()];
    geodesicPoly.setPath(path);
  }

  return {
    reinitMap: _reinitMap
  };
})();