from adaptor.model import CsvDbModel


class DataImporter(CsvDbModel):
    class Meta:
        dbModel = Airport
        delimiter = ","

dat_import = DataImporter.import_data(data=open('apps/airports/airports.dat'))
