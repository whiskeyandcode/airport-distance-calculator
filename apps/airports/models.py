from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible


DST_CHOICES = [
    ("E", "Europe"),
    ("A", "US/Canada"),
    ("S", "South America"),
    ("O", "Australia"),
    ("Z", "New Zealand"),
    ("N", "None"),
    ("U", "Unknown"),
]


class AirportManager(models.Manager):
    def us_airports(self):
        return super(AirportManager, self).get_queryset().filter(
            country="United States")


@python_2_unicode_compatible
class Airport(models.Model):
    airport_id = models.IntegerField()
    name = models.CharField(max_length=255)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    faa_code = models.CharField(max_length=5, blank=True, null=True)
    icao_code = models.CharField(max_length=5, blank=True, null=True)
    latitude = models.DecimalField(_('Latitude, negative is south, positive is north.'),
                                   max_digits=9, decimal_places=6)
    longitude = models.DecimalField(_('Longitude, negative is west, positive is east'),
                                    max_digits=9, decimal_places=6)
    altitude = models.FloatField(_("Altitude (in feet)"))
    timezone = models.FloatField(_("Hours offset from UTC"))
    dst = models.CharField(_("Daylight Savings Time"), max_length=10, choices=DST_CHOICES)
    tz_info = models.CharField(_("Timezone"), max_length=50)
    objects = AirportManager()

    def __str__(self):
        return self.name + ' - ' + self.city
