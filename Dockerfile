FROM python:2.7
ENV PYTHONUNBUFFERED 1

# Install dependencies
RUN apt-get update -y
RUN mkdir /code
ADD . /code/
WORKDIR /code
RUN pip install -r requirements.txt

